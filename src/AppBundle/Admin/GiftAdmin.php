<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class GiftAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->tab('Tab 1')
                ->with('Gift', ['class' => 'col-md-6'])
                    ->add('name', null, [
                        'label' => 'Name'
                    ])
                    ->add('price', null, [
                        'label' => 'Price'
                    ])
                    ->add('description')
                ->end()
                ->with('Participants', ['class' => 'col-md-6'])
                    ->add('addressee', 'entity', [
                        'class' => 'AppBundle\Entity\Addressee',
                    ])
                    ->add('buyer')
                ->end()
                
            ->end()
            ->tab('Tab 2')
                ->with('Shops', array('class' => 'col-md-6'))
                    ->add('shops', 'sonata_type_model', [
                        'by_reference' => false,
                        'expanded' => true,
                        'multiple' => true,
                        'label' => 'Shops',
                    ])
                ->end()
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('name')
            ->add('addressee', null, [], 'entity', [
                'class' => 'AppBundle\Entity\Addressee'
            ]);
    }

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier('name')
            ->add('price', 'currency', [
                'currency' => 'USD'
            ])
            ->add('addressee', null, [
                'editable' => true,
            ])
            ->add('_action', null, [
                'label' => 'Actions',
                'actions' => array(
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                )
            ])
        ;
    }
}