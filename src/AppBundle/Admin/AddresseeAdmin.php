<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class AddresseeAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->with("Addressee")
                ->add("name")
                // should not modify entity from the weak-side of the relation
                // ->add("gifts")
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter->add("name");
    }

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier("name")
            ->add("_action", null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => []
                ],
                'label' => 'Actions'
            ])
        ;
    }
}